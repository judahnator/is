<?php

if (!function_exists('is_')) {
    /**
     * Shortcut to the IS class.
     *
     * @param string $isWhat
     * @param $itemToCheck
     * @return mixed
     */
    function is_(string $isWhat, $itemToCheck) {
        return \judahnator\IS::$isWhat($itemToCheck);
    }
}