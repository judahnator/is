IS_ Library
===========

This is a super simple validation library.

The short version of what this library can do is summed up by this:

```php
<?php

require 'vendor/autoload.php';

use judahnator\IS;

// First register the items you want to test
IS::register('expected_input', function($argument) {
    // do your validation here
    return $argument === 'valid input';
});

// Now you can test using OOP
$a = IS::expected_input('valid input');

// You can also use a handy helper function
$b = is_('expected_input', 'valid input');

// And of course this will be false
$c = is_('expected_input', 'invalid input');

var_dump($a, $b, $c);
```