<?php

namespace judahnator;


class IS
{

    /**
     * The array of closure commands to check if an item is whatever.
     *
     * @var array
     */
    private $closures = [];

    // Singleton, mark these as private
    final private function __construct() {}
    final private function __clone() {}

    /**
     * Check if each of the given $arguments IS $name
     * @param string $name
     * @param $arguments
     * @return bool
     */
    public static function __callStatic(string $name, array $arguments): bool
    {
        // Make sure the command has been defined.
        // If it has not we obviously cannot run it.
        if (!array_key_exists($name, self::singleton()->closures)) {
            throw new \InvalidArgumentException("The command {$name} has not been defined.");
        }

        // If any of the arguments pass evaluate to false then return false
        foreach ($arguments as $itemToTest) {
            if (!self::singleton()->closures[$name]($itemToTest)) {
                return false;
            }
        }

        // otherwise everything checks out, return true
        return true;
    }

    /**
     * Registers a new IS function check
     * @param string $name
     * @param callable $callback
     */
    public static function register(string $name, callable $callback) {
        self::singleton()->closures[$name] = $callback;
    }

    /**
     * Removes a command from the IS class.
     *
     * @param string $name
     */
    public static function unregister(string $name) {
        if (!array_key_exists($name, self::singleton()->closures)) {
            throw new \InvalidArgumentException("Cannot unregister an unregistered command!");
        }
        unset(self::singleton()->closures[$name]);
    }

    /**
     * Removes all of the closures.
     */
    public static function reset() {
        self::singleton()->closures = [];
    }

    /**
     * Returns the singleton instance.
     *
     * @return IS
     */
    private static function singleton() {
        static $self = null;
        if (is_null($self)) {
            $self = new self();
        }
        return $self;
    }

}