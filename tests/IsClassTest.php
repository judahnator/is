<?php

namespace judahnator\Is\Tests;

use judahnator\IS;
use PHPUnit\Framework\TestCase;

class IsClassTest extends TestCase
{

    public function testRegisteringClosures() {
        IS::register('TestCase', function($itemToTest) {
            return $itemToTest instanceof TestCase;
        });
        $this->assertTrue(IS::TestCase($this));
    }

    public function testIsFunction() {
        IS::register('test', function() {
            return true;
        });
        $this->assertTrue(is_('test', 'unused'));
    }

    public function testCallingUnregisteredFunctions() {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage("The command foo has not been defined.");
        IS::foo('this should fail');
    }

    public function testFunctionsThatDoNotReturnBoolean() {
        IS::register('returnsInt', function() {
            return 5;
        });
        IS::register('returnsString', function() {
            return 'foo';
        });
        IS::register('returnsArray', function() {
            return ['test'];
        });
        IS::register('returnsObject', function() {
            return new \stdClass();
        });
        foreach (['returnsInt', 'returnsString', 'returnsArray', 'returnsObject'] as $itemToCall) {
            $this->assertTrue(IS::$itemToCall());
        }
    }

    public function testUnregisteringCommands() {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('The command commandToUnregister has not been defined.');
        IS::register('commandToUnregister', function() {});
        IS::unregister('commandToUnregister');
        IS::commandToUnregister();
    }

    public function testUnregisteringCommandsException() {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Cannot unregister an unregistered command');
        IS::register('commandToUnregister', function() {});
        IS::unregister('commandToUnregister');
        IS::unregister('commandToUnregister');
    }

    public function testResettingAllCommands() {
        IS::register('commandToUnregister', function() {});
        IS::register('anotherCommandToUnregister', function() {});
        IS::reset();
        try {
            IS::commandToUnregister();
        }catch (\Exception $exception) {
            $this->assertInstanceOf(\InvalidArgumentException::class, $exception);
        }
        try {
            IS::anotherCommandToUnregister();
        }catch (\Exception $exception) {
            $this->assertInstanceOf(\InvalidArgumentException::class, $exception);
        }
    }

}